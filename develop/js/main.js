/* jshint esversion: 6 */

import Vue from 'vue/dist/vue.min.js'
import App from './App.vue'

Vue.config.devtools = true
Vue.config.debug = true

new Vue({
	el: '#app',
	render: h => h(App)  
});

