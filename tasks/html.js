
// html

var $ 				= require('gulp-load-plugins')(),
	gulp 				= require('gulp'),
	combine 			= require('stream-combiner2').obj;

module.exports =  (options) => {

	return (callback) => {
		combine(
			gulp.src(options.src),
			gulp.dest(options.dest)
			).on('error', $.notify.onError((err) => {

				return {
					title: 'HTML',
					message: err.message
				}	
			}));
		callback();
	}
}