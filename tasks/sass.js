
// sass

var $ 				= require('gulp-load-plugins')(),
	gulp 			= require('gulp'),
	sassGlob 		= require('gulp-sass-glob'),
	cmq 			= require('gulp-group-css-media-queries'),
	combine 		= require('stream-combiner2').obj; // создает мультипоток


module.exports =  (options) => {

	var args = options.args, isDevelopment = true;
	
	if(['dev'] in args || ['prod'] in args) {
		isDevelopment = ['dev'] in args;
	}

	return (callback) => {

		gulp.src(options.src)
			.pipe(gulp.dest(options.sass));

		combine(
			gulp.src(options.src),
			sassGlob(),			
			$.sass({
				outputStyle: 'nested', //isDevelopment ? 'nested' : 'compressed', //nested, expanded, compact, compressed
				sourcemaps: isDevelopment
			}),
			$.autoprefixer({
				browsers: ['last 3 versions'],
            	cascade: false
			}),
            $.if(!isDevelopment, cmq({
                beautify: false
            })),
			gulp.dest(options.dest)
		).on('error', $.notify.onError((err) => {

			return {
				title: 'SASS',
				message: err.message
			};
		})); 
		callback();
	}
}