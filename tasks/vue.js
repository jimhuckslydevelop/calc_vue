
// vue

var $ 				= require('gulp-load-plugins')(), 
	gulp 			   = require('gulp'),	
	webpack			= require("webpack"),
   gutil          = require("gulp-util"),
   genv           = require("gulp-env");


module.exports = (options) => {

   var args = options.args, isDevelopment = true;
   
   if(['dev'] in args || ['prod'] in args) {
      isDevelopment = ['dev'] in args;
   }

   genv({
      vars: {
         NODE_ENV: isDevelopment ? 'development' : 'production'
      }
   });

   function onComplete(error, stats){
      if (error) {
         onError(error);
       } else if ( stats.hasErrors() ) {
         onError( stats.toString() );
       } else {
         return false;
            // onSuccess( stats.toString() );
       }
   }

   function onError(error) {
       let formatedError = new gutil.PluginError('react', error);
       $.notify({
         title: 'react',
         message: formatedError.message
       });
       return formatedError;
   }

	return (callback) => {
		webpack(require(options.path + '/vue.config.js'), onComplete);
		callback();
	} 
}