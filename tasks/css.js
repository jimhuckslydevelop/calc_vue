
// css

var $ 				= require('gulp-load-plugins')(),
	gulp 			= require('gulp'),
	combine 		= require('stream-combiner2').obj; // создает мультипоток

module.exports =  (options) => {

	var args = options.args, isDevelopment = true;
	
	if(['dev'] in args || ['prod'] in args) {
		isDevelopment = ['dev'] in args;
	}

	return (callback) => {		
		
		combine(
			gulp.src(options.src),
			$.concat('vendor.min.css'),
			$.if(!isDevelopment, $.cssnano()),
			gulp.dest(options.dest)
		).on('error', $.notify.onError((err) => {

			return {
				title: 'CSS',
				message: err.message,
				emitError: true
			}	
		}));
		
		callback();
	}
}