const path      = require('path'),
      webpack   = require('webpack'),
      NODE_ENV  = process.env.NODE_ENV;

var ExtractTextPlugin = require("extract-text-webpack-plugin"),
    UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    plugins = [];

plugins.push(new ExtractTextPlugin("./css/bundle.css"));

if(NODE_ENV == 'production') {
  plugins.push(new webpack.optimize.UglifyJsPlugin());
  plugins.push(new webpack.DefinePlugin({
    'process.env': {
        NODE_ENV: '"production"'
      }
  }));
  plugins.push(new UglifyJsPlugin());
}

module.exports = {
  entry: "./develop/js/main.js",
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        exclude: /node_modules/,
        options: {
          loaders: {
            css: ExtractTextPlugin.extract({
              use: 'css-loader',
              fallback: 'vue-style-loader'
            })
          }
        }
      }
    ]
  },
  plugins: plugins,
  output: {
    path: path.resolve(__dirname, './build/'),
    filename: 'js/bundle.js'
  }
};