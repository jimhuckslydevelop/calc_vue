
//======== Gulp ===========/

var gulp 				= require('gulp'),
	$ 					   = require('gulp-load-plugins')(), 
	combine 			   = require('stream-combiner2').obj, 
	sequence 			= require('run-sequence'),
	args        		= require('yargs').argv;

const 	dirname 			= require('path').resolve(__dirname);	
var 	   pkg 				= require('./package.json');
var 	   config 		   = require("./gulp.config.json");

//========= OPTIONS ===========//

var options = config.options;

options.name == null && (options.name = pkg.name);

var beforeBuildHTML = ['clean'];
	options.watch.sass && beforeBuildHTML.push('sass');
	options.watch.css && beforeBuildHTML.push('css');
	options.watch.images && beforeBuildHTML.push('images');
	options.watch.fonts && beforeBuildHTML.push('fonts');
	options.watch.json && beforeBuildHTML.push('json');
	options.watch.react && beforeBuildHTML.push('react');
	options.watch.vue && beforeBuildHTML.push('vue');
	options.watch.js && beforeBuildHTML.push('js');

//============== PATH ===========//

var path = config.path;

//============ lazyRequireTask ==========//

function lazyRequireTask(taskName, path, options){

	options = options || {};
	options.taskName = taskName;
	gulp.task(taskName, (callback) => {
		let task = require(path).call(this, options);
		return task(callback);
	});
};

//=========== HTML ============//

lazyRequireTask('html', './tasks/html.js', {
	src: path.develop.self + path.src.html,
	dest: path.build.self
});

//=========== JADE ============//

lazyRequireTask('jade', './tasks/jade.js', {
	src: path.develop.self + path.src.jade,
	dest: path.build.self
});

//========= SASS ==========//

lazyRequireTask('sass', './tasks/sass.js', {
	args: args,
	src: path.develop.sass + path.src.sass,
	sass: path.build.sass,
	dest: path.build.css
});

//=========== CSS ===========//

lazyRequireTask('css', './tasks/css.js', {
	args: args,
	src: path.develop.css + path.src.css,	
	dest: path.build.css
});

//========= IMAGES ==========//

lazyRequireTask('images', './tasks/images.js', {
	args: args,
	src: path.develop.images + path.src.images,	
	dest: path.build.images
});

// ============= JS ===========//

lazyRequireTask('js', './tasks/scripts.js', {
	args: args,
	src_lib: path.develop.js.lib + path.src.js,	
	src: path.develop.js._this + path.src.js,
	dest: path.build.js
});

//============= REACT ===========//

lazyRequireTask('react', './tasks/react.js', {
	args: args,
	path: dirname
});

//=============== VUE =============//

lazyRequireTask('vue', './tasks/vue.js', {
	args: args,
	path: dirname
});

//============ FONTS ===========//

lazyRequireTask('fonts', './tasks/fonts.js', {	
	src: path.develop.fonts + path.src.all,
	dest: path.build.fonts
});

//============ JSON ===========//

lazyRequireTask('json', './tasks/json.js', {	
	src: path.develop.self + path.src.json,
	dest: path.build.self
});

//========== CLEAN CACHE ===========//

gulp.task('clean', () => $.cache.clearAll());

//========== BUILD ================//

gulp.task('build', function(){
	
	if(['dev'] in args || ['prod'] in args) {
		options.isDevelopment = ['dev'] in args;
	}

	options.watch.jade && gulp.start('jade', beforeBuildHTML);
	options.watch.html && gulp.start('html', beforeBuildHTML);
});

//============ DEFAULT ========//

gulp.task('default', (callback) => {
	
	if(['dev'] in args || ['prod'] in args) {
		isDevelopment = ['dev'] in args;
	}
	gulp.start("build");
	callback();
});